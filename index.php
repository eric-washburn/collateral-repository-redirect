<?php
  //run the following code 100% of the time
  //all routes under http://files.asset.microfocus.com/ domain should run the below code
  $DAMRedirects = new DAMRedirects();
  $DAMRedirects->selfTest();
  $DAMRedirects->main();

  /**
   * Used for http://files.asset.microfocus.com/ domain
   * it used to be pointed to rackspace CDN, now it needs to be a regular server
   * Redirect to new files based off a CSV
   * run selfTest() to make the class test itself
   */
  class DAMRedirects{
    private $csv_filename = 'assets.csv';
    private $default_redirect = 'https://software.microfocus.com/resources';
    private $dam_url_column = 5;
    private $redirect_url_column = 9;
    public $request_url = null;
    public $redirects = null;
    public $redirectsEnabled = true;
    public $test_status = true;

    public function main(){
      $this->getRedirectData();
      $request_url = $this->getRequestURL();
      $this->redirectFromURL($request_url);
    }

    /**
     * Get redirect data from CSV
     */
    private function getRedirectData(){
      if(!empty($this->redirects)){
        return $this->redirects;
      }

      $csvFile = file($this->csv_filename);
      $redirects = [];
      foreach ($csvFile as $line_number=>$line) {
        //skip headers
        if($line_number < 1){
          continue;
        }
        $valid = true;

        $row = str_getcsv($line);
        $from = str_replace(['http://files.asset.microfocus.com', 'files.asset.microfocus.com', ' ', 'DONE'], '', $row[$this->dam_url_column]);
        $from_parts = parse_url($from);
        $from = $from_parts['path'];

        $to = $row[$this->redirect_url_column];
        if(strpos($to, 'Obsoleted by Client') !== false){
          $valid = false;
        }else{
          $to = $this->addhttps($to);
        }

        if(!empty($from) && !empty($to) && $valid){
          $redirects[$from] = $to;
        }
      }

      #var_dump($redirects); exit;
      $this->redirects = $redirects;
      return $redirects;
    }

    /**
     * get the request URL from $_SERVER
     */
    public function getRequestURL(){
      if(!empty($this->request_url)){
        return $this->request_url;
      }

      $request_url = false;
      $request_url = 'http://files.asset.microfocus.com/8836/en/8836.pdf';

      if(!empty($_SERVER['REQUEST_URI'])){
        $request_uri = $_SERVER['REQUEST_URI'];
        //remove query strings
        $request_uri_parts = parse_url($request_uri);
        $request_url = $request_uri_parts['path'];
      }

      $this->request_url = $request_url;
      return $request_url;
    }

    /**
     * redirect to appropriate URL based on request url
     * set $this->redirectsEnabled to false to just throw a debug message instead
     */
    function redirectFromURL($request_url, $redirects = null){
      $redirect = null;
      if(empty($redirects)){
        $redirects = $this->redirects;
      }

      if(!empty($redirects[$request_url])){
        $redirect = $redirects[$request_url];
        $this->redirect($redirect, $request_url);

      }else{
        //url was not found. just go to resources
        $redirect = $this->default_redirect;
        $this->redirect($this->default_redirect, $request_url);
      }

      return $redirect;
    }

    /**
     * Have the class test itself
     */
    public function selfTest(){
      $this->getRedirectData();
      $this->redirectsEnabled = false;

      //verify that redirect data exists
      if(empty($this->redirects)){
        $this->assertTrue(false, $this->csv_filename.' was unable to be read correctly!!!');
      }

      foreach($this->redirects as $from=>$to){
        $valid = true;
        $error = '';

        if(empty($from) || empty($to)){
          $valid = false;
          $error = 'from or to is empty';
        }

        if(strpos($to, 'https://') !== 0 && strpos($to, 'http://') !== 0){
          $valid = false;
          $error = 'to is not a real URL';
        }

        if(strpos($from, '/') !== 0){
          $valid = false;
          $error = 'from does not start with "/"';
        }

        $this->assertTrue($valid, 'Error on CSV Data FROM - '.$from.' - TO - '.$to.' ERROR - '.$error);
      }

      //test out a bunch of URL's
      $test_urls = [
        //'from' => 'to',
        //'' => '',
        '/8720/en/8720.pdf' => 'https://intra.microfocus.net/docrep/documents/sm8ldv958y/icbc_turkey_bank_a_s_cpp.pdf',
        '/8692/en/8692.pdf' => 'https://www.microfocus.com/media/success-story/washington_state_department_of_transportation_ss.pdf',
        '/8655/en/8655.pdf' => 'https://www.microfocus.com/media/success-story/channel_4_ss.pdf',
        '/6784/en/6784.pdf' => 'https://www.microfocus.com/media/success-story/east_gippsland_water_css.pdf',
        '/8573/en/8573.pdf' => 'https://www.microfocus.com/media/success-story/capax_discovery_inc_ss.pdf',
        '/3480/en/3480.pdf' => 'https://www.microfocus.com/media/brief/onesource_intranet_accelerator_for_sharepoint_brief.pdf',
        'not_found' => $this->default_redirect,
      ];

      foreach ($test_urls as $from=>$to){
        $test = $this->redirectFromURL($from);
        $this->assertTrue($to == $test, 'Expected '.$to.', but '.$from.' redirected to '.$test.'
');
      }

      //re-enable redirects for other functions
      $this->redirectsEnabled = true;

      if(!$this->test_status){
        $this->printMessage('SELF TEST FAILED. Investigate the code in the collateral-repository-redirect repo');
        exit(1);
      }
    }

    /**
     * assertTrue test, used for self tests and printing messages
     * utility function
     */
    public function assertTrue($input, $message)
    {
        if(empty($input)) {
            $this->printMessage($message);
            $this->test_status = false;
        }
    }

    /**
     * print a message, used for debugging
     * utility function
     */
    public function printMessage($message){
      echo $message.'
';
    }

    /**
     * redirect, or give a debug message
     * utility function
     */
    function redirect($redirect, $request_url = ''){
      if($this->redirectsEnabled){
        // actually redirect
        header('Location: '.$redirect);
        exit(0);
      }else{
        //$this->printMessage('DEBUG redirect() - The site would have redirected FROM '.$request_url.' TO '.$redirect);
      }
    }

    /**
     * Add HTTPS to a URL if it doesn't exist
     * modified to be https
     * https://stackoverflow.com/questions/2762061/how-to-add-http-if-it-doesnt-exists-in-the-url#2762083
     */
    function addhttps($url) {
      if (!empty($url) && !preg_match("~^(?:f|ht)tps?://~i", $url)) {
        $url = "https://" . $url;
      }
      return $url;
    }





    /**
     * Data from PNX on languages
     */
    /*function _node_getLanguageData(){
    //created by copy pasting from node and converting to PHP
    //find & replace on { to [
    //find & replace on } to ]
    //find & replace on : to =>
    $data = [
      'en-us'=>  [
        'www_country'=>  '+++United+States',
        'www_code'=> 'en-us',
        'code'=>  'en-us',
        'cms_code'=>  'en',
      ],
      'de-de'=> [
        'www_country'=>  '+++Deutschland',
        'www_code'=> 'de-de',
        'code'=>  'de-de',
        'cms_code'=>  'de',
      ],
      'es-es'=> [
        'www_country'=>  '+++Mexico',
        'www_code'=> 'es-es',
        'code'=>  'es-es',
        'cms_code'=>  'es',
      ],
      'fr-fr'=> [
        'www_country'=>  '+++Belgique',
        'www_code'=> 'fr-fr',
        'code'=>  'fr-fr',
        'cms_code'=>  'fr',
      ],
      'it-it'=> [
        'www_country'=>  '+++Italia',
        'www_code'=> 'it-it',
        'code'=>  'it-it',
        'cms_code'=>  'it',
      ],
      'ja-jp'=> [
        'www_country'=>  '+++United+States',
        'www_code'=> 'ja-jp',
        'code'=>  'ja-jp',
        'cms_code'=>  'ja',
      ],
      'pt-br'=> [
        'www_country'=>  '+++Brasil',
        'www_code'=> 'pt-br',
        'code'=>  'pt-br',
        'cms_code'=>  'pt-br',
      ],
      'zh-hans' => [
        'www_country'=>  '+++%E4%B8%AD%E5%9B%BD',
        'www_code'=> 'zh-cn',
        'code'=>  'zh-cn',
        'cms_code'=>  'zh-hans',
      ],
      'zh-tw' => [
        'www_country'=>  '+++%E9%A6%99%E6%B8%AF',
        'www_code'=> 'zh-tw',
        'code'=>  'zh-cn',
        'cms_code'=>  'zh-hans',
      ],
      'ko-kr'=> [
        'www_country'=>  '+++United+States',
        'www_code'=> 'ko-kr',
        'code'=>  'ko-kr',
        'cms_code'=>  'ko',
      ],
      'ru-ru'=> [
        'www_country'=>  '+++United+States',
        'www_code'=> 'ru-ru',
        'code'=>  'ru-ru',
        'cms_code'=>  'ru',
      ],
    ];

    return $data;
  }*/
}